package com.captton.ejerciciosclase1;

import java.util.Scanner;
import java.util.ArrayList;

public class EjerciciosPrimeraClase {

	public static void main(String[] args) {
	
		//---------------------------------------------Ejercicio 1.
		
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Ingrese su nombre:");
		String nombre = sc1.nextLine();
		System.out.println("Ingrese su edad:");
		int edad = sc1.nextInt();
		System.out.println("Usted es "+nombre+" y tiene "+edad+" a�os.");
		
		//---------------------------------------------Ejercicio 2.
		Scanner sc2 = new Scanner(System.in);
		System.out.println("Ingrese un numero: ");
		int aux = sc2.nextInt();
		switch(aux) {
		case 1:
			System.out.println("Usted ingreso el numero UNO");
			break;
		case 2:
			System.out.println("Usted ingreso el numero DOS");
			break;
		case 3:
			System.out.println("Usted ingreso el numero TRES");
			break;
		case 4:
			System.out.println("Usted ingreso el numero CUATRO");
			break;
		case 5:
			System.out.println("Usted ingreso el numero CINCO");
			break;
		case 6:
			System.out.println("Usted ingreso el numero SEIS");
			break;
		case 7:
			System.out.println("Usted ingreso el numero SIETE");
			break;
		case 8:
			System.out.println("Usted ingreso el numero OCHO");
			break;
		case 9:
			System.out.println("Usted ingreso el numero NUEVE");
			break;
		default:
			System.out.println("Usted no ingreso un numero del 1 al 9");
		}
		//---------------------------------------------Ejercicio 3
		Scanner sc3 = new Scanner(System.in);
		System.out.println("Ingrese el primer numero");
		int a = sc3.nextInt();
		System.out.println("Ingrese el primer numero");
		int b = sc3.nextInt();
		if(a<b)
			System.out.println(a+", "+b);
		else
			System.out.println(b+", "+a);
		//---------------------------------------------Ejercicio 4
		Scanner sc4 = new Scanner(System.in);
		System.out.println("Ingrese un numero:");
		int parOimpar = sc4.nextInt();
		if(parOimpar%2==0)
			System.out.println("El numero ingresado es par.");
		else
			System.out.println("El numero ingresado es impar.");
		//---------------------------------------------Ejercicio 5
		Scanner sc5 = new Scanner(System.in);
		System.out.println("Ingrese la base:");
		int base = sc5.nextInt();
		System.out.println("Ingrese el exponente:");
		int exponente = sc5.nextInt();
		int aux2 = 0;
		int resWhile = 1;
		while(aux2<exponente) {
			resWhile = resWhile*base;
			aux2++;
		}
		System.out.println(resWhile);
		int resFor = 1;
		for(int i=0; i<exponente ;i++) {
			resFor=resFor*base;
		}
		System.out.println(resFor);
		//---------------------------------------------Ejercicio 6
		ArrayList<String> personas = new ArrayList<String>();
		int indice;
		personas.add("Juan");
		personas.add("Esteban");
		personas.add("Jose");
		personas.add("Ezequiel");
		personas.add("Pedro");
		personas.add("Eduardo");
		if(personas.contains("Pedro")) {
			indice = personas.indexOf("Pedro");
			System.out.println("Pedro esta en la posicion "+indice);
		}else {
			System.out.println("Pedro no esta en la lista.");
		}
		//---------------------------------------------Ejercicio 7
		Scanner sc7 = new Scanner(System.in);
		System.out.println("Ingrese un valor");
		int valor = sc7.nextInt();
		int puntaje;
		if(valor<0 || valor>9) 
			System.out.println("El valor ingresado debe estar entre 0 y 9");
		if(valor<4 && valor >0) {
			puntaje = valor*10;
		}else {
			puntaje = valor*1000;
		}
		System.out.println("El puntaje extra que le corresponde es de: "+puntaje+" puntos");
		//---------------------------------------------Ejercicio 8
		int[] array = new int[10];
		System.out.println("Ingrese el primer valor");
		Scanner sc8 = new Scanner(System.in);
		int ingreso = sc8.nextInt();
		array[0]=ingreso;
		for(int i=0; i<9 ;i++)
		{
			array[i]=ingreso+1;
			ingreso++;
		}
		for(int i=0; i<9 ;i++)
		{
			System.out.println(array[i]);
		}
			
		}

}
